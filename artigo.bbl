\begin{thebibliography}{}

\bibitem[Barker et~al. 2009]{barker2009using}
Barker, K., Davis, K., Hoisie, A., Kerbyson, D., Lang, M., Pakin, S., and
  Sancho, J. (2009).
\newblock Using performance modeling to design large-scale systems.
\newblock {\em IEEE Computer}, 42(11):42--49.

\bibitem[Che et~al. 2009]{rodiniaBS}
Che, S., Sheaffer, J.~W., Boyer, M., Szafaryn, L.~Z., Wang, L., and Skadron, K.
  (2009).
\newblock A characterization of the rodinia benchmark suite.

\bibitem[Danalis 2010]{danalis2010scalable}
Danalis, A. (2010).
\newblock {The scalable heterogeneous computing (SHOC) benchmark suite}.
\newblock In {\em Proceedings of the 3rd Workshop on General-Purpose
  Computation on Graphics Processing Units}, pages 63--74. ACM.

\bibitem[Glaskowsky 2009]{glaskowsky2009nvidia}
Glaskowsky, P.~N. (2009).
\newblock {NVIDIA’s Fermi: the first complete GPU computing architecture}.
\newblock {\em NVIDIA Corporation, September}.

\bibitem[Huang et~al. 2009]{huang2009energy}
Huang, S., Xiao, S., and Feng, W.-c. (2009).
\newblock {On the energy efficiency of graphics processing units for scientific
  computing}.
\newblock In {\em Parallel \& Distributed Processing, 2009. IPDPS 2009. IEEE
  International Symposium on}, pages 1--8. IEEE.

\bibitem[{INTEL Inc} 2012]{intel2012}
{INTEL Inc} (2012).
\newblock {Intel Arrandale Whitepaper}.

\bibitem[{NVIDIA Inc} 2012]{gtx2012680}
{NVIDIA Inc} (2012).
\newblock {GeForce GTX 680: The fastest, most efficient GPU ever built.
  Whitepaper}.

\bibitem[Padoin et~al. 2013]{padoin2013evaluatingCLUSTER}
Padoin, E.~L., Pilla, L.~L., Boito, F.~Z., Kassick, R.~V., Velho, P., and
  Navaux, P. O.~A. (2013).
\newblock {Evaluating application performance and energy consumption on hybrid
  CPU+GPU architecture}.
\newblock {\em Cluster Computing}, 16(3):511--525.

\bibitem[Rahimi et~al. 2013]{evergreen}
Rahimi, A., Benini, L., and Gupta, R.~K. (2013).
\newblock Aging-aware compiler-directed vliw assignment for gpgpu
  architectures.
\newblock In {\em DAC'13}, New York, NY, USA. ACM.

\bibitem[Younge et~al. 2010]{younge2010efficient}
Younge, A., von Laszewski, G., Wang, L., Lopez-Alarcon, S., and Carithers, W.
  (2010).
\newblock Efficient resource management for cloud computing environments.
\newblock In {\em International Conference on Green Computing}, pages 357--364.
  IEEE.

\end{thebibliography}
